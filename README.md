# udamola

## Udamola
Udamola is a Universal ata Modelling Language.

## Description

Udamola is work in progress a regular expression rule based tool.
Udamola procesess `uda` files which consist of somewhat markdown like
paragraphs and sentences that describe data entities and their relations.
Udamola implemented in Go language and can also be used as a Go library for
data modeling.

The intent is for udamola to generate Go language, protobuffer, diagrams,
SQL, database code, etc, using templates or built in functionality, to make
it easier to have a single source of truth for data processing and handling.

## Badges
Todo.

## Installation

go install gitlab.com/useful-go/udamola@latest

## Usage
Todo.

## Support

Feel free to use the issue tracker for questions or suggestions.

## Roadmap
Todo

## Contributing
Please make a pull request.

## Authors and acknowledgment

* BDM: author

## License
This project is available under the MIT license.

## Project status

This project is currently work in progress.

