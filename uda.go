package udamola

import "os"
import "io"
import "bufio"
import "strings"
import "unicode/utf8"
import "fmt"

type Position struct {
	File   string `json:"file,omitempty"`
	Line   int    `json:"line"`
	Column int    `json:"column,omitempty"`
}

func (p Position) String() string {
	if p.File == "" {
		return fmt.Sprintf("%d", p.Line)
	} else if p.Column == 0 {
		return fmt.Sprintf("%s:%d", p.File, p.Line)
	} else {
		return fmt.Sprintf("%s:%d:%d", p.File, p.Line, p.Column)
	}
}

type Paragraph struct {
	Position  `json:"position"`
	Sentences []Sentence `json:"sentences"`
}

type Sentence struct {
	Position `json:"position"`
	Text     string `json:"text"`
}

type ParseError struct {
	Position `json:"position"`
	Message  string `json:"message"`
}

func (p ParseError) Error() string {
	return fmt.Sprintf("%s:%s", p.Position, p.Message)
}

type Uda struct {
	Paragraphs []Paragraph `json:"paragraphs"`
}

type parseState int

const (
	parseStateNormal parseState = iota
	parseStateEndLine
	parseStateEscape
)

func Parse(r io.Reader, file string) (*Uda, error) {
	var ch rune
	var err error
	var (
		state     = parseStateNormal
		pos       = Position{file, 1, 1}
		paragraph = &Paragraph{}
		sentence  = &Sentence{}
		result    = &Uda{}
	)

	addParagraph := func() {
		if len(paragraph.Sentences) > 0 {
			result.Paragraphs = append(result.Paragraphs, *paragraph)
		}
		paragraph = &Paragraph{Position: pos}
	}

	addSentence := func() {
		if len(sentence.Text) > 0 {
			paragraph.Sentences = append(paragraph.Sentences, *sentence)
		}
		sentence = &Sentence{Position: pos}
	}
	br := bufio.NewReader(r)
	for ch, _, err = br.ReadRune(); err == nil; ch, _, err = br.ReadRune() {
		pos.Column++
		switch ch {
		case '\n':
			pos.Column = 1
			pos.Line++
			switch state {
			case parseStateNormal:
				state = parseStateEndLine
				addSentence()
			case parseStateEndLine:
				state = parseStateNormal
				addParagraph()
			case parseStateEscape:
				state = parseStateNormal
			}
		case ',', ':', ';':
			state = parseStateEscape
			sentence.Text += string(ch)
		case '.':
			state = parseStateNormal
			addSentence()
		default:
			if !utf8.ValidRune(ch) {
				return nil, ParseError{pos, "Character not valid unicode: " + string(ch)}
			}
			state = parseStateNormal
			sentence.Text += string(ch)
		}
	}
	if err != io.EOF {
		return result, ParseError{pos, err.Error()}
	}
	addParagraph()
	return result, nil
}

func ParseString(s, file string) (*Uda, error) {
	r := strings.NewReader(s)
	return Parse(r, file)
}

func ParseFile(file string) (*Uda, error) {
	fin, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	return Parse(fin, file)
}
