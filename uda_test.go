package udamola

import "testing"

// TestParseSuccess tests successful parsing of valid input text into a UDA object.
func TestParseSuccess(t *testing.T) {
	aver := NewAver(t)
	inputStr := `This is some sample text with commas,
colons, and periods.
It should be parsed correctly.`
	// Use JSON to compare the expected value easier.
	expectJSON := `{"paragraphs":[{"position":{"line":0},"sentences":[{"position":{"line":0},"text":"This is some sample text with commas,colons, and periods"},{"position":{"line":3,"column":1},"text":"It should be parsed correctly"}]}]}`

	output, err := ParseString(inputStr, "")
	aver.NoErr(err)
	aver.JSONEqual(expectJSON, output)
}
