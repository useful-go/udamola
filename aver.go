package udamola

import "testing"
import "reflect"
import "runtime"
import "fmt"
import "encoding/json"

type Aver struct {
	*testing.T
}

func NewAver(t *testing.T) *Aver {
	return &Aver{t}
}

func (a Aver) context(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if ok {
		return fmt.Sprintf("%s:%d", file, line)
	}
	return "??:??"
}

func (a *Aver) True(b bool) {
	a.Helper()
	if !b {
		a.Fatalf("aver: not true")
	}
}

func (a *Aver) Equal(expected, observed any) {
	a.Helper()
	if !reflect.DeepEqual(expected, observed) {
		a.Fatalf("aver: not equal: %#v <=> %#v", expected, observed)
	}
}

func (a *Aver) NoErr(err error) {
	a.Helper()
	if err != nil {
		a.Fatalf("aver: error: %s", err.Error())
	}
}

func (a *Aver) Err(err error) {
	a.Helper()
	if err == nil {
		a.Fatalf("aver: expected error, but no error")
	}
}

func (a *Aver) JSONEqual(expectedJSON string, observed any) {
	observedBytes, err := json.Marshal(observed)
	if err != nil {
		a.Errorf("unexpected error: %q", err)
	}
	observedJSON := string(observedBytes)
	if expectedJSON != observedJSON {
		a.Fatalf("aver: not equal:\n%s\n%s", expectedJSON, observedJSON)
	}
}
